package rest_gen

import "io"

type Resource struct {
	Package     string
	PackageRoot string
	Name        string
	UrlParam    []Field
	QueryParam  []Field
	JsonFields  []Field
}

type FIELD_TYPE int

const (
	INT FIELD_TYPE = iota
	STRING
	FLOAT
	TIMESTAMP
)

type REQUEST_TYPE int

const (
	READ REQUEST_TYPE = iota
	CREATE
	UPDATE
	DELETE
	QUERY
)

type CONTROL int

const (
	HIDDEN CONTROL = iota
	READONLY
	IMMUTABLE
	MUTABLE
)

type Field struct {
	Name     string
	Kind     FIELD_TYPE
	Control  CONTROL
	Required bool
}

func (f Field) IsHidden() bool {
	return f.Control == HIDDEN
}

func (f Field) IsReadOnly() bool {
	return f.Control == READONLY
}

func (f Field) IsImmutable() bool {
	return f.Control == IMMUTABLE
}

func (f Field) IsMutable() bool {
	return f.Control == MUTABLE
}

func (f Field) IsWritable() bool {
	return f.Control == IMMUTABLE || f.Control == MUTABLE
}

type Generator func(resouce Resource, server io.Writer, client io.Writer, test io.Writer)