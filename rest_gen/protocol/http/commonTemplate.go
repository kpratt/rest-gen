package genHttp


const PackageHeader string = `
package {{.Package}}

`
const ModelTemplate string = `

import "time"

type {{.Name}} struct {
	id             int // read-only
	{{range .JsonFields}}
		{{.Name}} {{fieldType .}}
	{{end}}
}
`

const CommonTemplate string = `

import (

	"github.com/julienschmidt/httprouter"
	model "bitbucket.org/kpratt/rest-gen/sample_project/generated/pkg"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"fmt"
	"log"
	"errors"
	"bytes"
	"time"
)

type {{.Name}}Model struct {
	resource model.{{.Name}}
}

type {{.Name}}DefaultParams struct {
	{{range .JsonFields}}
		{{if not .Required}}
			{{.Name}} {{fieldType .}}
		{{end}}
	{{end}}
}

type {{.Name}}ClientModel struct {
	id             int
	{{range .JsonFields}}
		{{if not .IsHidden}}
			{{.Name}} {{fieldType .}}
		{{end}}
	{{end}}
}

type {{.Name}}CreateModel struct {
	{{range .JsonFields}}
		{{if and (not .IsReadOnly) (not .IsHidden)}}
			{{.Name}} {{fieldType .}}
		{{end}}
	{{end}}
}

type {{.Name}}UpdateModel struct {
	{{range .JsonFields}}
		{{if .IsMutable}}
			{{.Name}} {{fieldType .}}
		{{end}}
	{{end}}
}

func New{{.Name}}(params {{.Name}}CreateModel, defaults {{.Name}}DefaultParams) {{.Name}}Model {
	// for required and not read-only / hidden
	resource := model.{{.Name}}{}
	{{range .JsonFields}}
		{{if and (.Required) (not .IsWritable)}}
			resource.{{.Name}} = params.{{.Name}}
		{{end}}
	{{end}}

	{{range .JsonFields}}
		{{if and (not .Required) (.IsWritable) }}
			if params.{{.Name}} != nil {
				resource.{{.Name}} = params.{{.Name}}
			} else {
				x := *defaults.{{.Name}}
				resource.{{.Name}} = &x
			}
		{{end}}
	{{end}}
	// for optional and not read-only / hidden

	return {{.Name}}Model{resource}
}

func (resource *{{.Name}}Model) UpdateWith(params {{.Name}}UpdateModel) {
	// for not read-only / immutable / hidden
	{{range .JsonFields}}
		{{if .IsMutable}}
			{{if .Required}}
				resource.resource.{{.Name}} = params.{{.Name}}
			{{else}}
				if params.{{.Name}} != nil {
					resource.resource.{{.Name}} = params.{{.Name}}
				}
			{{end}}
		{{end}}
	{{end}}
}

func (resource *{{.Name}}Model) ForClient() (visible {{.Name}}ClientModel) {
	// for not hidden
	{{range .JsonFields}}
		{{if not .IsHidden}}
			visible.{{.Name}} = resource.resource.{{.Name}}
		{{end}}
	{{end}}
	return
}

type {{.Name}}Controller struct {

}

func jsonEncode{{.Name}}(r {{.Name}}ClientModel) ([]byte, error) {
	//return []byte(fmt.Sprintf("{\"immutableFeild\": %d, \"readOnlyFeild\": %d, \"requiredFeild\": \"%s\", \"optionalFeild\": %d}", r.immutableFeild, r.readOnlyFeild, r.requiredFeild, *r.optionalFeild)), nil
	return []byte{}, nil
}
`
