package genHttp


const DeleteTemplate string = `

type Delete{{.Name}} func(id int) error

func (controller *{{.Name}}Controller) Delete{{.Name}}(f Delete{{.Name}}) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

		bytes, err := ioutil.ReadAll(r.Body)

		var stub map[string]interface{}
		err = json.Unmarshal(bytes, &stub)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		var id float64 = 0
		_, ok := stub["id"]
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"id param was missing.\"}")))
		}

		id, ok = stub["id"].(float64)
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"id param not an integer.\"}")))
		}

		err = f(int(id))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		w.WriteHeader(http.StatusOK)

	}
}
`
