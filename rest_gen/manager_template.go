package rest_gen

const ManagmentTemplate string = `
import model "bitbucket.org/kpratt/rest-gen/sample_project/generated/pkg"

type {{.Name}}Mgr struct {
	service model.{{.Name}}Service
}

func (mgr {{.Name}}Mgr) Read() Read{{.Name}} {
  return func(id int) ({{.Name}}ClientModel, error) {
  }
}

func (mgr {{.Name}}Mgr) Create() Create{{.Name}}Model {
	return func(resource {{.Name}}CreateModel) ({{.Name}}ClientModel, error) {
	}
}

func (mgr {{.Name}}Mgr) Update() Update{{.Name}}Model {
	return func(resource {{.Name}}UpdateModel) ({{.Name}}ClientModel, error) {
	}
}

func (mgr {{.Name}}Mgr) Delete() Delete{{.Name}} {
	return func(id int) error {
	}
}

func (mgr {{.Name}}Mgr) List() List{{.Name}}Models {
	return func(limit int, offset int, sortby []string, other map[string]interface{}) ([]{{.Name}}ClientModel, error) {
	}
}
`