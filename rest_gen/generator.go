package rest_gen

import (
	"text/template"
	"os"
	"strings"
	"bitbucket.org/kpratt/rest-gen/rest_gen/protocol/http"
	"bitbucket.org/kpratt/rest-gen/rest_gen/protocol/http_client"
	"bitbucket.org/kpratt/rest-gen/rest_gen/persistance/mem"
	"errors"
	"fmt"
	"bitbucket.org/kpratt/rest-gen"
	"bitbucket.org/kpratt/rest-gen/rest_gen/persistance"
)

type TemplateMetaDat struct {
	dir       string
	postfix   string
	handle    *os.File
	templates [][2]string
}

func GenerateGoModule(resources []rest_gen.Resource, dir string) error {
	os.MkdirAll(dir, os.FileMode(0755))

	dirs := []TemplateMetaDat{
		TemplateMetaDat{dir: "/pkg/custom/router/", postfix: "_routing.go", templates: [][2]string{
			[2]string{" Package Declaration", genHttp.PackageHeader},
		}},
		TemplateMetaDat{dir: "/pkg/custom/manager/", postfix: "_manager.go", templates: [][2]string{
			[2]string{" Package Declaration", genHttp.PackageHeader},
			[2]string{" Manager Template", ManagmentTemplate},
		}},
		TemplateMetaDat{dir: "/pkg/", postfix: "_model.go", templates: [][2]string{
			[2]string{" Package Declaration", genHttp.PackageHeader},
			[2]string{" Model Struct", genHttp.ModelTemplate},
			[2]string{" Service Interface", persistance.ServiceTemplate},
			[2]string{" Serialization", persistance.SerialTemplate},
		}},
		TemplateMetaDat{dir: "/pkg/http/server/", postfix: "_controller.go", templates: [][2]string{
			[2]string{" Package Declaration", genHttp.PackageHeader},
			[2]string{" Common Code Server template", genHttp.CommonTemplate},
			[2]string{" HTTP GET Server template", genHttp.ReadTemplate},
			[2]string{" HTTP POST Server template", genHttp.CreateTemplate},
			[2]string{" HTTP PUT Server template", genHttp.UpdateTemplate},
			[2]string{" HTTP DELETE Server template", genHttp.DeleteTemplate},
			[2]string{" HTTP List Server template", genHttp.ListTemplate},
		}},
		TemplateMetaDat{dir: "/pkg/http/client/", postfix: "_client.go", templates: [][2]string{
			[2]string{" Common Code Client template", genHttpClient.ClientCommonTemplate},
			[2]string{" HTTP GET Client template", ""},
			[2]string{" HTTP POST Client template", ""},
			[2]string{" HTTP PUT Client template", ""},
			[2]string{" HTTP DELETE Client template", ""},
			[2]string{" HTTP List Client template", ""},
		}},
		TemplateMetaDat{dir: "/pkg/http/tests/", postfix: "_test.go", templates: [][2]string{
			[2]string{" Common Code Test template", testCommonTemplate},
			[2]string{" HTTP GET Test template", ""},
			[2]string{" HTTP POST Test template", ""},
			[2]string{" HTTP PUT Test template", ""},
			[2]string{" HTTP DELETE Test template", ""},
			[2]string{" HTTP List Test template", ""},
		}},
		TemplateMetaDat{dir: "/pkg/db/mem/", postfix: "_mem.go", templates: [][2]string{
			[2]string{" Package Declaration", genHttp.PackageHeader},
			[2]string{" In Memory Data Service template", mem.ServiceTemplate},
		}},
		TemplateMetaDat{dir: "/pkg/db/postgres/", postfix: "_postgres.go", templates: [][2]string{
			[2]string{" Package Declaration", genHttp.PackageHeader},
			//[2]string{" Postgress Data Service template", postgres.ServiceTemplate},
		}},
		TemplateMetaDat{dir: "/ui/react/", postfix: "_react.js", templates: [][2]string{}},
		TemplateMetaDat{dir: "/ui/redux/", postfix: "_redux.js", templates: [][2]string{}},
	}

	var err error
	for _, resource := range resources {
		for i, xdir := range dirs {
			os.MkdirAll(dir + xdir.dir, os.FileMode(0644))
			path := dir + xdir.dir + strings.ToLower(resource.Name) + xdir.postfix
			dirs[i].handle, err = os.OpenFile(path, os.O_WRONLY | os.O_TRUNC | os.O_CREATE, os.FileMode(0644))
			if err != nil {
				return errors.New(err.Error() + " " + path)
			}
			defer func(handle *os.File, path string) {
				err := handle.Close()
				if err != nil {
					fmt.Println(err.Error() + " Closing target file " + path)
				}
			}(dirs[i].handle, path)
		}

		err := GenerateGoCode(resource, dirs)
		if err != nil {
			return err
		}
	}
	return nil
}

func GenerateGoCode(resource rest_gen.Resource, temps []TemplateMetaDat) error {
	funcs := template.FuncMap{
		"fieldType": templateFuncFeildType,
		"assertType": templateFuncFeildAssertType,
		"constructType": templateFuncFeildConstructtType,
	}

	for _, temp := range temps {
		for i := 0; i < len(temp.templates); i++ {
			name := resource.Name + temp.templates[i][0]
			t, err := template.New(name).Funcs(funcs).Parse(temp.templates[i][1])
			if err != nil {
				return errors.New(err.Error() + " creating template " + name)
			}
			err = t.Execute(temp.handle, resource)
			if err != nil {
				return errors.New(err.Error() + " executing (" + resource.Package + " into " + temp.templates[i][1] + ") " + name)
			}
		}
	}

	return nil
}

func templateFuncFeildType(field rest_gen.Field) string {
	if field.Required {
		switch field.Kind {
		case rest_gen.STRING: return "string"
		case rest_gen.INT: return "int"
		case rest_gen.FLOAT: return "float64"
		case rest_gen.TIMESTAMP: return "time.Time"
		}
	} else {
		switch field.Kind {
		case rest_gen.STRING: return "*string"
		case rest_gen.INT: return "*int"
		case rest_gen.FLOAT: return "*float64"
		case rest_gen.TIMESTAMP: return "*time.Time"
		}
	}
	return "<Unknown Type Specified>"
}

func templateFuncFeildAssertType(field rest_gen.Field) string {
	switch field.Kind {
	case rest_gen.STRING: return "string"
	case rest_gen.INT: return "float64"
	case rest_gen.FLOAT: return "float64"
	case rest_gen.TIMESTAMP: return "string"
	}
	return "<Unknown Type Specified>"
}

func templateFuncFeildConstructtType(field rest_gen.Field, varName string) string {
	switch field.Kind {
	case rest_gen.STRING: return "string(" + varName + ")"
	case rest_gen.INT: return "int(" + varName + ")"
	case rest_gen.FLOAT: return "float64(" + varName + ")"
	case rest_gen.TIMESTAMP: return "time.Time(" + varName + ")"
	}
	return "<Unknown Type Specified>"
}