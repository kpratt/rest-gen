package mem

import (
	"sort"
	"strings"
)

type InMemoryService struct {
	dataType string
	fields   []string
	idGen    int
	data     map[int]map[string]interface{}
	get      chan getReq
	list     chan listReq
	insert   chan insertReq
	update   chan updateReq
	delete   chan deleteReq
}

type getReq struct {
	id   int
	resp chan map[string]interface{}
}

type listReq struct {
	offset int
	length int
	sortby string
	asc    bool
	resp   chan []map[string]interface{}
}

type insertReq struct {
	record map[string]interface{}
	resp   chan map[string]interface{}
}

type updateReq struct {
	id     int
	record map[string]interface{}
	resp   chan map[string]interface{}
}

type deleteReq struct {
	id   int
	resp chan error
}

func NewInMemoryService(datatype string, fields []string) InMemoryService {
	service := InMemoryService{
		dataType: datatype,
		fields: fields,
		data: make(map[int]map[string]interface{}),
		idGen: 0,
		get: make(chan getReq),
		list: make(chan listReq),
		insert: make(chan insertReq),
		update: make(chan updateReq),
		delete: make(chan deleteReq),
	}

	go func(service InMemoryService) {
		select {
		case req := <-service.get:
			req.resp <- service.data[req.id]
		case req := <-service.list:
			sortKey := req.sortby
			if sortKey == "" {
				sortKey = "id"
			}
			keys := make([]int, len(service.data))
			for k := range service.data {
				keys = append(keys, k)
			}
			vs := sortable{
				sortKey: sortKey,
				slice: keys,
				data: service.data,
			}
			sort.Sort(vs)

			records := make([]map[string]interface{}, req.length)
			for i:=0; i<req.length && req.offset + i < vs.Len(); i++ {
				records[i] = service.data[vs.slice[i+req.offset]]
			}
			req.resp <- records

		case req := <-service.insert:
			id := service.idGen
			service.idGen += 1
			record := req.record
			record["id"] = id

			service.data[id] = record

			req.resp <- service.data[id]
		case req := <-service.update:
			record := service.data[req.id]
			for k := range req.record {
				record[k] = req.record[k]
			}
			service.data[req.id] = record
			req.resp <- record
		case req := <-service.delete:
			delete(service.data, req.id)
			req.resp <- nil
		}
	}(service)

	return service
}



func (service InMemoryService) Type() string {
	return service.dataType
}

func (service InMemoryService) Fields() []string {
	return service.fields
}

func (service InMemoryService) Get(id int) (map[string]interface{}, error) {
	c := make(chan map[string]interface{})
	service.get <- getReq{
		id: id,
		resp: c,
	}
	return <-c, nil
}

func (service InMemoryService) List(offset int, length int, sortBy string, asc bool) []map[string]interface{} {
	c := make(chan []map[string]interface{})
	service.list <- listReq{
		offset: offset,
		length: length,
		sortby: sortBy,
		asc: asc,
		resp: c,
	}
	return <-c
}

func (service InMemoryService) Insert(record map[string]interface{}) (map[string]interface{}, error) {
	c := make(chan map[string]interface{})
	service.insert <- insertReq{
		record: record,
		resp: c,
	}
	return <-c, nil
}

func (service InMemoryService) Update(id int, record map[string]interface{}) (map[string]interface{}, error) {
	c := make(chan map[string]interface{})
	service.update <- updateReq{
		id: id,
		record: record,
		resp: c,
	}
	return <-c, nil
}

func (service InMemoryService) Delete(id int) error {
	c := make(chan error)
	service.delete <- deleteReq{
		id: id,
		resp: c,
	}
	return <-c
}

////////////////////////////////////////////////////////////

type sortable struct {
	sortKey string
	slice   []int
	data    map[int]map[string]interface{}
}

func (x sortable) Len() int {
	return len(x.slice)
}
func (x sortable) Less(i, j int) bool {
	left := x.data[x.slice[i]][x.sortKey]
	switch right := x.data[x.slice[j]][x.sortKey].(type) {
	case int:
		return left.(int) < right
	case string:
		return strings.Compare(left.(string), right) < 0
	default:
		return true
	}

}
func (x sortable) Swap(i, j int) {
	temp := x.slice[i]
	x.slice[i] = x.slice[j]
	x.slice[j] = temp
}
