package mem

const ServiceTemplate = `
import mem "bitbucket.org/kpratt/rest-gen/rest_gen/persistance/mem"
import model "bitbucket.org/kpratt/rest-gen/sample_project/generated/pkg"

type {{.Name}}InMemoryService struct {
	db mem.InMemoryService
}

func New{{.Name}}InMemoryService() {{.Name}}InMemoryService {
	return {{.Name}}InMemoryService {
		db: mem.NewInMemoryService("{{.Name}}", []string{
			{{range .JsonFields}}
			"{{.Name}}",
			{{end}}
		}),
	}
}

func (service {{.Name}}InMemoryService) Get(pk int) (model.{{.Name}}, error) {
	resource, err := service.db.Get(pk)
	m := model.Person{}
	m.FromMap(resource)
	return m, err
}

func (service *{{.Name}}InMemoryService) Insert(x model.{{.Name}}) (model.{{.Name}}, error) {
	resource, err := service.db.Insert(x.ToMap())
	m := model.Person{}
	m.FromMap(resource)
	return m, err
}

func (service *{{.Name}}InMemoryService) Update(x model.{{.Name}}) (model.{{.Name}}, error) {
	resource, err := service.db.Update(x.PrimaryKey(), x.ToMap())
	m := model.Person{}
	m.FromMap(resource)
	return m, err
}

func (service *{{.Name}}InMemoryService) Delete(pk int) error {
	return service.db.Delete(pk)
}
`
