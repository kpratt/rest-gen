package persistance

const SerialTemplate = `
func (m {{.Name}}) PrimaryKey() int {
	return m.id
}

func (m {{.Name}}) ToMap() (ret map [string]interface{}) {
	ret = make(map[string]interface{})
	{{range .JsonFields}}
		ret["{{.Name}}"] = interface{}(m.{{.Name}})
	{{end}}
	return
}

func (m *{{.Name}}) FromMap(fields map [string]interface{}) {
	{{range .JsonFields}}
		m.{{.Name}} = fields["{{.Name}}"].({{fieldType .}})
	{{end}}
}

`