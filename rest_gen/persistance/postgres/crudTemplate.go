package postgres


const DatabaseTemplate = `
type PostgresDB struct {
	conn driver.Conn
}

func (db PostgresDB) Get(table string, pk int) map[string]interface{} {
	rows, err := db.conn.Query("SELECT * FROM $1 WHERE id = $2", table, pk)
	cols := rows.Columns()
	row := make([]interface{}, len(cols))
	err := rows.Next(row)

	if err == io.EOF {
		return nil
	}

	m := make(map[string]interface{})
	for i:=0; i<len(cols); i++ {
		m[cols[i]] = row[i]
	}
	return m
}

func (m *PostgresDB) Insert(model SerializableRecord) {

}

func (m *PostgresDB) Update(model SerializableRecord) {

}

func (m *PostgresDB) Delete(model SerializableRecord) {

}
`

const ServiceTemplate = `

type {{.Name}}InMemoryService struct {
	db *PostgresDb
	table string
}

func (service {{.Name}}PostgresService) Get(pk int) {{.Name}} {
	model := new({{.Name}})
	model.fromMap(service.db.Get(service.table, pk))
	return model
}

func (service {{.Name}}PostgresService) Insert(model {{.Name}}) {
}

func (service {{.Name}}PostgresService) Update(model {{.Name}}) {
}

func (service {{.Name}}PostgresService) Delete(pk int) {
}
`