package persistance

type Service interface {
	Type() string
	Fields() []string
	Get(int) (map[string]interface{}, error)
	List(offset int, length int, sortBy string, asc bool) []map[string]interface{}
	Insert(map[string]interface{}) (map[string]interface{}, error)
	Update(int, map[string]interface{}) (map[string]interface{}, error)
	Delete(int) error
}

