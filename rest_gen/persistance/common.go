package persistance

const dbTemplate = `

type DB interface {
	Get(table string, pk int) map[string]interface{}
	Insert(model SerializableRecord)
	Update(model SerializableRecord)
	Delete(table string, pk int)
}

`

const ServiceTemplate = `

type {{.Name}}Service interface {
	Get(pk int) {{.Name}}
	Insert(model {{.Name}})
	Update(model {{.Name}})
	Delete(pk int)
}

`
