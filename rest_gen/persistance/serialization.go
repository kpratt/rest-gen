package persistance

type SerializableRecord interface{
	Serializable
	Record
}

type Record interface{
	PrimaryKey() int
	Type() string
}
type Serializable interface{
	ToMap() (map [string]interface{})
	FromMap (map [string]interface{})
}


