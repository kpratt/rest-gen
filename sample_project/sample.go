package main

import (
	"fmt"
	"os"
	"bitbucket.org/kpratt/rest-gen"
	gen "bitbucket.org/kpratt/rest-gen/rest_gen"
)

func main() {
	resource := rest_gen.Resource{
		Package:    "generated_sample",
		PackageRoot: "bitbucket.org/kpratt/rest-get/sample_project/generated/pkg",
		Name:       "Person",
		UrlParam:   []rest_gen.Field{},
		QueryParam: []rest_gen.Field{},
		JsonFields: []rest_gen.Field{
			{
				Name:     "FirstName",
				Required: true,
				Control:  rest_gen.IMMUTABLE,
				Kind:     rest_gen.STRING,
			},
			{
				Name:     "LastName",
				Required: true,
				Control:  rest_gen.MUTABLE,
				Kind:     rest_gen.STRING,
			},
			{
				Name:    "MiddleName",
				Control: rest_gen.MUTABLE,
				Kind:    rest_gen.STRING,
			},
			{
				Name:    "CreatedAt",
				Control: rest_gen.HIDDEN,
				Kind:    rest_gen.TIMESTAMP,
			},
			{
				Name:     "DOB",
				Required: true,
				Control:  rest_gen.IMMUTABLE,
			},
			{
				Name:    "LastLogin",
				Control: rest_gen.READONLY,
			},
		},
	}

	err := gen.GenerateGoModule([]rest_gen.Resource{resource }, "./generated")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(1)
	}
}
