package generated_sample

import (
	model "bitbucket.org/kpratt/rest-gen/sample_project/generated/pkg"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type PersonModel struct {
	resource model.Person
}

type PersonDefaultParams struct {
	MiddleName *string

	CreatedAt *time.Time

	LastLogin *int
}

type PersonClientModel struct {
	id int

	FirstName string

	LastName string

	MiddleName *string

	DOB int

	LastLogin *int
}

type PersonCreateModel struct {
	FirstName string

	LastName string

	MiddleName *string

	DOB int
}

type PersonUpdateModel struct {
	LastName string

	MiddleName *string
}

func NewPerson(params PersonCreateModel, defaults PersonDefaultParams) PersonModel {
	// for required and not read-only / hidden
	resource := model.Person{}

	if params.MiddleName != nil {
		resource.MiddleName = params.MiddleName
	} else {
		x := *defaults.MiddleName
		resource.MiddleName = &x
	}

	// for optional and not read-only / hidden

	return PersonModel{resource}
}

func (resource *PersonModel) UpdateWith(params PersonUpdateModel) {
	// for not read-only / immutable / hidden

	resource.resource.LastName = params.LastName

	if params.MiddleName != nil {
		resource.resource.MiddleName = params.MiddleName
	}

}

func (resource *PersonModel) ForClient() (visible PersonClientModel) {
	// for not hidden

	visible.FirstName = resource.resource.FirstName

	visible.LastName = resource.resource.LastName

	visible.MiddleName = resource.resource.MiddleName

	visible.DOB = resource.resource.DOB

	visible.LastLogin = resource.resource.LastLogin

	return
}

type PersonController struct {
}

func jsonEncodePerson(r PersonClientModel) ([]byte, error) {
	//return []byte(fmt.Sprintf("{\"immutableFeild\": %d, \"readOnlyFeild\": %d, \"requiredFeild\": \"%s\", \"optionalFeild\": %d}", r.immutableFeild, r.readOnlyFeild, r.requiredFeild, *r.optionalFeild)), nil
	return []byte{}, nil
}

type ReadPerson func(id int) (PersonClientModel, error)

func (controller *PersonController) ReadPerson(f ReadPerson) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

		bytes, err := ioutil.ReadAll(r.Body)

		var stub map[string]interface{}
		err = json.Unmarshal(bytes, &stub)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		var id float64 = 0
		_, ok := stub["id"]
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"id param was missing.\"}")))
		}

		id, ok = stub["id"].(float64)
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"id param not an integer.\"}")))
		}

		resource, err := f(int(id))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		output, err := jsonEncodePerson(resource)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		_, err = w.Write(output)
		if err != nil {
			log.Printf("Failed to write response [ %s ]", err)
		}
	}
}

type CreatePersonModel func(resource PersonCreateModel) (PersonClientModel, error)

func (controller *PersonController) CreatePerson(f CreatePersonModel) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		var ok bool

		bites, err := ioutil.ReadAll(r.Body)
		var resource PersonCreateModel
		var jsonObj map[string]interface{}
		err = json.Unmarshal(bites, &jsonObj)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While decodeing an Person [ %s ] raw data was [ %s ] ", err, string(bites))
		}

		var missing []string = make([]string, 0, 4)
		// for required feilds

		if _, ok = jsonObj["FirstName"]; !ok {
			missing = append(missing, "FirstName")
		}

		if _, ok = jsonObj["LastName"]; !ok {
			missing = append(missing, "LastName")
		}

		if _, ok = jsonObj["DOB"]; !ok {
			missing = append(missing, "DOB")
		}

		// TODO custom error type
		if len(missing) > 0 {
			buff := new(bytes.Buffer)
			buff.WriteString("Feild(s) were missing [")
			for i := 0; i < len(missing); i++ {
				buff.WriteString(missing[i])
				buff.WriteString(", ")
			}
			buff.WriteString("]")
			err = errors.New(buff.String())

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		var wrongType []string = make([]string, 0, 4)

		// for all required feilds

		if field, ok := jsonObj["FirstName"].(string); ok {
			x := string(field)
			resource.FirstName = x
		} else {
			wrongType = append(wrongType, "FirstName: string")
		}

		if field, ok := jsonObj["LastName"].(string); ok {
			x := string(field)
			resource.LastName = x
		} else {
			wrongType = append(wrongType, "LastName: string")
		}

		if field, ok := jsonObj["DOB"].(float64); ok {
			x := int(field)
			resource.DOB = x
		} else {
			wrongType = append(wrongType, "DOB: int")
		}

		// for all optional feilds

		if _, ok = jsonObj["MiddleName"]; ok {
			if field, ok := jsonObj["MiddleName"].(string); ok {
				x := string(field)
				resource.MiddleName = &x
			} else {
				wrongType = append(wrongType, "MiddleName: *string")
			}
		}

		// TODO custom error type
		if len(wrongType) > 0 {
			buff := new(bytes.Buffer)
			buff.WriteString("Type error. Feilds should have type: [")
			for i := 0; i < len(wrongType); i++ {
				buff.WriteString(wrongType[i])
				buff.WriteString(", ")
			}
			buff.WriteString("]")
			err = errors.New(buff.String())

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		visible, err := f(resource)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While encoding an Person post create [ %s ]", err)
		}

		output, err := jsonEncodePerson(visible)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While encoding an Person post create [ %s ]", err)
		}

		_, err = w.Write(output)
		if err != nil {
			log.Printf("Failed to write create response [ %s ]", err)
		}
	}
}

type UpdatePersonModel func(resource PersonUpdateModel) (PersonClientModel, error)

func (controller *PersonController) UpdatePerson(f UpdatePersonModel) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		var ok bool

		bites, err := ioutil.ReadAll(r.Body)
		var resource PersonUpdateModel
		var jsonObj map[string]interface{}
		err = json.Unmarshal(bites, &jsonObj)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While decodeing an Person [ %s ] raw data was [ %s ] ", err, string(bites))
		}

		var missing []string = make([]string, 0, 4)
		// for required feilds

		if _, ok = jsonObj["LastName"]; !ok {
			missing = append(missing, "LastName")
		}

		// TODO custom error type
		if len(missing) > 0 {
			buff := new(bytes.Buffer)
			buff.WriteString("Feild(s) were missing [")
			for i := 0; i < len(missing); i++ {
				buff.WriteString(missing[i])
				buff.WriteString(", ")
			}
			buff.WriteString("]")
			err = errors.New(buff.String())

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		var wrongType []string = make([]string, 0, 4)

		// for all required feilds

		if field, ok := jsonObj["LastName"].(string); ok {
			x := string(field)
			resource.LastName = x
		} else {
			wrongType = append(wrongType, "LastName: string")
		}

		// for all optional feilds

		if _, ok = jsonObj["MiddleName"]; ok {
			if field, ok := jsonObj["MiddleName"].(string); ok {
				x := string(field)
				resource.MiddleName = &x
			} else {
				wrongType = append(wrongType, "MiddleName: *string")
			}
		}

		// TODO custom error type
		if len(wrongType) > 0 {
			buff := new(bytes.Buffer)
			buff.WriteString("Type error. Feilds should have type: [")
			for i := 0; i < len(wrongType); i++ {
				buff.WriteString(wrongType[i])
				buff.WriteString(", ")
			}
			buff.WriteString("]")
			err = errors.New(buff.String())

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		visible, err := f(resource)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While encoding an Person post create [ %s ]", err)
		}

		output, err := jsonEncodePerson(visible)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While encoding an Person post create [ %s ]", err)
		}

		_, err = w.Write(output)
		if err != nil {
			log.Printf("Failed to write create response [ %s ]", err)
		}
	}
}

type DeletePerson func(id int) error

func (controller *PersonController) DeletePerson(f DeletePerson) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

		bytes, err := ioutil.ReadAll(r.Body)

		var stub map[string]interface{}
		err = json.Unmarshal(bytes, &stub)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		var id float64 = 0
		_, ok := stub["id"]
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"id param was missing.\"}")))
		}

		id, ok = stub["id"].(float64)
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"id param not an integer.\"}")))
		}

		err = f(int(id))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		w.WriteHeader(http.StatusOK)

	}
}

type ListPersonModels func(limit int, offset int, sortby []string, other map[string]interface{}) ([]PersonClientModel, error)

func (controller *PersonController) ListPerson(f ListPersonModels) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		var ok bool
		var wrongType []string = make([]string, 0, 4)
		var jsonObj map[string]interface{}

		bites, err := ioutil.ReadAll(r.Body)
		err = json.Unmarshal(bites, &jsonObj)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		var limit int = 50
		if _, ok = jsonObj["limit"]; ok {
			if field, ok := jsonObj["limit"].(float64); ok {
				limit = int(field)
			} else {
				wrongType = append(wrongType, "limit: int")
			}
			delete(jsonObj, "limit")
		}

		var offset int = 0
		if _, ok = jsonObj["offset"]; ok {
			if field, ok := jsonObj["offset"].(float64); ok {
				offset = int(field)
			} else {
				wrongType = append(wrongType, "offset: int")
			}
			delete(jsonObj, "offset")
		}

		var sortBy []string = []string{"id"}
		if _, ok = jsonObj["sortBy"]; ok {
			if field, ok := jsonObj["sortBy"].([]string); ok { // pretty sure i cant cast this, will need to use []interface{}
				if len(field) > 0 {
					sortBy = field
				}
			} else {
				wrongType = append(wrongType, "sortBy: [string]")
			}
			delete(jsonObj, "sortBy")
		}

		resources, err := f(limit, offset, sortBy, jsonObj)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		buff := new(bytes.Buffer)
		buff.Write([]byte("["))
		for i := 0; i < len(resources); i++ {
			if i != 0 {
				buff.Write([]byte(", "))
			}
			output, err := jsonEncodePerson(resources[i])
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
			}
			buff.Write(output)
		}
		buff.Write([]byte("]"))

		_, err = w.Write([]byte(buff.String()))
		if err != nil {
			log.Printf("Failed to write response [ %s ]", err)
		}
	}
}
