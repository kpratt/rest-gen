package generated_sample

import "time"

type Person struct {
	id int // read-only

	FirstName string

	LastName string

	MiddleName *string

	CreatedAt *time.Time

	DOB int

	LastLogin *int
}

type PersonService interface {
	Get(pk int) Person
	Insert(model Person)
	Update(model Person)
	Delete(pk int)
}

func (m Person) PrimaryKey() int {
	return m.id
}

func (m Person) ToMap() (ret map[string]interface{}) {
	ret = make(map[string]interface{})

	ret["FirstName"] = interface{}(m.FirstName)

	ret["LastName"] = interface{}(m.LastName)

	ret["MiddleName"] = interface{}(m.MiddleName)

	ret["CreatedAt"] = interface{}(m.CreatedAt)

	ret["DOB"] = interface{}(m.DOB)

	ret["LastLogin"] = interface{}(m.LastLogin)

	return
}

func (m *Person) FromMap(fields map[string]interface{}) {

	m.FirstName = fields["FirstName"].(string)

	m.LastName = fields["LastName"].(string)

	m.MiddleName = fields["MiddleName"].(*string)

	m.CreatedAt = fields["CreatedAt"].(*time.Time)

	m.DOB = fields["DOB"].(int)

	m.LastLogin = fields["LastLogin"].(*int)

}
