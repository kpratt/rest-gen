package generated_sample

import model "bitbucket.org/kpratt/rest-gen/sample_project/generated/pkg"

type PersonMgr struct {
	service model.PersonService
}

func (mgr PersonMgr) Read() ReadPerson {
	return func(id int) (PersonClientModel, error) {
	}
}

func (mgr PersonMgr) Create() CreatePersonModel {
	return func(resource PersonCreateModel) (PersonClientModel, error) {
	}
}

func (mgr PersonMgr) Update() UpdatePersonModel {
	return func(resource PersonUpdateModel) (PersonClientModel, error) {
	}
}

func (mgr PersonMgr) Delete() DeletePerson {
	return func(id int) error {
	}
}

func (mgr PersonMgr) List() ListPersonModels {
	return func(limit int, offset int, sortby []string, other map[string]interface{}) ([]PersonClientModel, error) {
	}
}
