package generated_sample

import mem "bitbucket.org/kpratt/rest-gen/rest_gen/persistance/mem"
import model "bitbucket.org/kpratt/rest-gen/sample_project/generated/pkg"

type PersonInMemoryService struct {
	db mem.InMemoryService
}

func NewPersonInMemoryService() PersonInMemoryService {
	return PersonInMemoryService{
		db: mem.NewInMemoryService("Person", []string{

			"FirstName",

			"LastName",

			"MiddleName",

			"CreatedAt",

			"DOB",

			"LastLogin",
		}),
	}
}

func (service PersonInMemoryService) Get(pk int) (model.Person, error) {
	resource, err := service.db.Get(pk)
	m := model.Person{}
	m.FromMap(resource)
	return m, err
}

func (service *PersonInMemoryService) Insert(x model.Person) (model.Person, error) {
	resource, err := service.db.Insert(x.ToMap())
	m := model.Person{}
	m.FromMap(resource)
	return m, err
}

func (service *PersonInMemoryService) Update(x model.Person) (model.Person, error) {
	resource, err := service.db.Update(x.PrimaryKey(), x.ToMap())
	m := model.Person{}
	m.FromMap(resource)
	return m, err
}

func (service *PersonInMemoryService) Delete(pk int) error {
	return service.db.Delete(pk)
}
